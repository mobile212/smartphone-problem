import 'package:flutter/material.dart';

class SettingsView extends StatelessWidget {
  const SettingsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: ListView.builder(itemBuilder: ((context, index) {
              return ListTile(
                leading: const Icon(Icons.notifications),
                title: Text('Setting$index'),
                trailing: Switch(
                  value: true,
                  onChanged: (_) {},
                ),
              );
            })),
          )
        ],
      ),
    );
  }
}
